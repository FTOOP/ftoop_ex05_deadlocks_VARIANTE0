package ch.ffhs.ftoop.p1.deadlock;

import java.util.concurrent.locks.ReentrantLock;

/**
 * Aufgabe: Dieses Programm demonstriert einen Deadlock. Lassen Sie dieses
 * Programm mehrfach laufen und schauen Sie, was passiert.
 * 
 * a) Erklaeren Sie das Verhalten des Programms. b) Korrigieren Sie das
 * Programm, so dass es sich korrekt verhaelt. Verändern Sie dabei nicht die
 * Klasse Friend.
 * 
 * a) 
 *    *************************************
 *    ************ Das Problem ************
 *    *************************************
 *    Das Schl�sselwort "synchronized" vor "bow()" und "bowBack()" bezieht sich auf das ganze 
 *    Friend-Objekt. Wenn die eine synchronized-Methode aufgerufen wird, kann nicht gleichzeitig eine andere
 *    synchronized-Methode im selben Friend-Objekt aufgerufen werden.
 *    Die Threads "alphonseThread" und "gastonThread" blockieren sich somit gegenseitig,
 *    weil die "bow()" Methode intern noch die "bowBack()" Methode des Freundes aufrufen moechte, was aus oben genanntem Grund nicht moeglich ist.
 *    
 *    *************************************
 * 	  ************ Details... *************
 *    *************************************
 *    Die Klasse "Friend" besitzt eine synchronized Methode "bow(Friend bower)".
 *    Die Methode greift erst auf die "getName()"-Methode eines anderen Friend-Objekts zu. 
 *    Danach wird die synchronized Methode "bowBack(this)" des anderen Friend-Objekts ausgefuehrt, wobei das Objekt selbst als Parameter uebergeben wird.
 *    
 *    Die Klasse "Deadlock" erstellt zwei Objekte der Klasse "Thread".
 *    Innerhalb des Threads "alphonseThread" fuehrt die "run()"-Methode die "bow(alphonse)"-Methode von Friend gaston aus.
 *    Innerhalb des Threads "gastonThread" fuehrt die "run()"-Methode die "bow(gaston)"-Methode von Friend alphonse aus.
 *    
 *    In der for-Schleife der Klasse "DeadlockTest.java" klappt dieses Hin- und Her- auf meinem PC 1 oder 0 mal.
 *    Sp�testens danach kommt das Programm in den "Starvation"-Status.

 *    ---------------------- gastonThread starting. ----------------------
 *    Alphonse: Gaston  has bowed to me!
 *    Gaston: Alphonse has bowed back to me!
 *    ---------------------- alphonseThread starting. ----------------------
 *    Gaston: Alphonse  has bowed to me!
 *    Alphonse: Gaston has bowed back to me!
 *    ------------------------------------------------------------
 *    ---------------------- Threads joined ----------------------
 *    
 *    ---------------------- gastonThread starting. ----------------------
 *    ---------------------- alphonseThread starting. ----------------------
 *    Alphonse: Gaston  has bowed to me!
 *    Gaston: Alphonse  has bowed to me!
 *    *** STARVATION! ***
 *    
 *    b) 
 *    *************************************
 *    ************* L�sungen **************
 *    *************************************
 *    0) Am sinnvollsten waere es, nicht die ganzen Methoden "bow()" und "bowBack()" synchronized zu deklarieren,
 *       sondern nur den Teil System.out.println(). Aber das ist ja laut Aufgabenstellung keine Option.
 *       
 *    1) Am einfachsten die Abfolge von start(); und join(); wie folgt veraendern:
 *    	 gastonThread.start();
 *    	 gastonThread.join();
 *       alphonseThread.start();
 *	     alphonseThread.join();
 *	     Somit wird immer erst gewartet, bis der erste Thread beendet ist, bevor der zweite Thread gestartet wird.
 *       Mit so einer Vorgehensweise kann man sich das Multithreading natuerlich sparen, weil es nichts bringt.
 *	  
 *    2) Inden (2015) fuehrt in seinem Buch auf S. 499ff sogenannte Locks auf der Grundlage von java.util.concurrent.locks.ReentrantLock ein.
 *       Diese Variante schein uns ausgekluegelter zu sein. Ueber ein fuer alle Threads sichtbares Lock-Objekt wird innerhalb der
 *       "run()"-Methoden geprueft, ob der "Zutritt" in den Block moeglich ist. Bzw. wird versucht, den Lock zu setzen. Ist der Lock frei,
 *       wird die "bow()" Methode ausgefuehrt. Ist der Lock nicht frei, wird gewartet. Somit wird das Multithreading kurzzeitig unterbrochen.
 *       Es koennen nicht beide bow() Methoden in den Threads gleichzeitig ausgefuehrt werden. Dies verhindert Deadlocks.
 *    
 * @author bele
 * 
 */
public class Deadlock {

	void doStuff() throws InterruptedException {
		final Friend alphonse = new Friend("Alphonse");
		final Friend gaston = new Friend("Gaston");
		final ReentrantLock lockobj = new ReentrantLock();
		Thread gastonThread = new Thread(new Runnable() {
			public void run() {
				lockobj.lock();            // Explizit Lock anfordern
				try {
					alphonse.bow(gaston);  // Kritischer Bereich
				} finally {
					lockobj.unlock();      // Explizit Lock freigeben
				}
			}
		}, "Gaston");
		

		Thread alphonseThread = new Thread(new Runnable() {
			public void run() {
				lockobj.lock();            // Explizit Lock anfordern
				try {
					gaston.bow(alphonse);  // Kritischer Bereich
				} finally {
					lockobj.unlock();      // Explizit Lock freigeben
				}

			}
		}, "Alphonse");
		gastonThread.start();
		alphonseThread.start();
		
		gastonThread.join();
		alphonseThread.join();
		
	}

	public static void main(String[] args) throws InterruptedException {
		Deadlock d = new Deadlock();
		d.doStuff();
		
	}
}

class Friend {
	private final String name;

	public Friend(String name) {
		this.name = name;
	}

	public String getName() {
		return this.name;
	}

	public synchronized void bow(Friend bower) {
		System.out.format("%s: %s" + " has bowed to me!%n", this.name, bower.getName());
		bower.bowBack(this);
	}

	public synchronized void bowBack(Friend bower) {
		System.out.format("%s: %s" + " has bowed back to me!%n", this.name, bower.getName());
	}
}