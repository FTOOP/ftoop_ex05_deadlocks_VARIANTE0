package ch.ffhs.ftoop.p1.deadlock;

//import org.junit.Test;
import student.TestCase;

public class DeadlockTest extends TestCase {

	public void testDoStuff() throws InterruptedException {

		int nTimes = 4;
		for (int i = 0; i < nTimes; i++) {
			Deadlock d = new Deadlock();
			d.doStuff();
		}
		
		// Mit einem jUnit test wird geprueft, dass der String "\n" ein 4-faches der Anzahl Schleifendurchlaeufe vorkommt.
		// Dies ist der Fall, also wurden alle Methoden korrekt ausgefuehrt.
		String hist = systemOut().getHistory();
		int l1 = hist.length();
		int l2 = hist.replace("\n", "").length();
		assertEquals(l1 - l2, nTimes*4);
		
		
	}

}